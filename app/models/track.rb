class Track < ApplicationRecord

  validates_presence_of :title, :artist, :album
end

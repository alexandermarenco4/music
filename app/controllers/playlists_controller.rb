class PlaylistsController < ApplicationController

  def index
    @playlists = Playlist.all  ##Obtener todas las listas de reproduccioon
  end

  def show
    @playlists = Playlist.find(params[:id])
  end

  def new
    @playlists = Playlist.new
  end

  def create
    @playlists = Playlist.new(playlist_params)
    if @playlists.save
      redirect_to @playlists
    else
      render :new
    end
  end

  private

  def playlist_params
    params.require(:playlist).permit(:name, :number_of_votes)
  end
end
